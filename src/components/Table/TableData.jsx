import React, { Component } from "react";
import moment from "moment";
import { removeEvents } from "../../service/eventService";

class TableData extends Component {
	constructor(props) {
		super(props);
		this.state = { hasEvent: false };
		this.deleteEvent = this.deleteEvent.bind(this);
	}

	deleteEvent(event) {
		removeEvents(event.target.id).then(() => {
			this.props.fetchEvents();
			this.setState({ ...this.state, hasEvent: false });
		});
	}

	componentWillMount() {
		this.setState({ ...this.state, hasEvent: true });
	}

	render() {
		const { year, month, day, hour } = this.props;
		const eventDate = moment
			.utc([year, month, day, hour])
			.format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");

		const event = this.props.data?.find((event) => {
			return event.date === eventDate;
		});

		if (event && this.state.hasEvent === false) {
			return this.state.hasEvent;
		}

		return (
			<td
				id={day + "-" + hour}
				className={event ? "bg-info align-items-center p-0" : null}
			>
				{event?.title}
				<button
					id={event?._id}
					className="btn btn-sm btn-ligth text-end"
					hidden={!event?.title}
					onClick={(event) => this.deleteEvent(event)}
				>
					X
				</button>
			</td>
		);
	}
}

export default TableData;
