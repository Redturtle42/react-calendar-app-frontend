import React, { Component, Fragment } from "react";
import TableHeader from "./TableHeader";
import TableRows from "./TableRows";

class Table extends Component {
	render() {
		return (
			<Fragment>
				<TableHeader weekDays={this.props.weekDays} />
				<TableRows
					data={this.props.data}
					hours={this.props.hours}
					currentWeek={this.props.currentWeek}
					fetchEvents={this.props.fetchEvents}
				/>
			</Fragment>
		);
	}
}

export default Table;
