import React, { Component } from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import { getEvents } from "../../service/eventService";
import { getUsers } from "../../service/userService";
import "../../stylesheets/table.css";
import Table from "./Table";
import SelectOneUser from "../User/SelectOneUser";

const HOURS = ["9", "10", "11", "12", "13", "14", "15", "16", "17", "18"];

class WeekPicker extends Component {
	constructor(props) {
		super(props);
		this.state = {
			users: [],
			selectedUser: "",
			usersName: "",
			data: null,
			currentWeek: moment().clone().startOf("isoWeek"),
		};
		this.fetchEvents = this.fetchEvents.bind(this);
		this.handleSelectedUserChange = this.handleSelectedUserChange.bind(this);
	}

	fetchEvents = () => {
		getEvents().then((event) => {
			const filteredEvents = event.filter((item) => {
				return item.users.includes(this.state.selectedUser.value);
			});
			this.setState({
				...this.state,
				data: this.state.selectedUser.value ? filteredEvents : event,
			});
		});
	};

	fetchUsers = () => {
		getUsers().then((data) => {
			this.setState({ ...this.state, users: data });
		});
	};

	componentDidMount() {
		this.fetchEvents();
		this.fetchUsers();
	}

	getCurrentWeek() {
		const days = [""];
		for (let i = 0; i < 5; i++) {
			days.push(
				moment(this.state.currentWeek).add(i, "days").format("MMMM Do, dddd")
			);
		}
		return days;
	}

	prevWeek = () => {
		this.setState({
			currentWeek: moment(this.state.currentWeek)
				.clone()
				.startOf("isoWeek", "currentWeek")
				.add(-7, "days"),
		});
	};

	nextWeek = () => {
		this.setState({
			currentWeek: moment(this.state.currentWeek)
				.clone()
				.startOf("isoWeek", "currentWeek")
				.add(7, "days"),
		});
	};

	handleSelectedUserChange(selectedUser) {
		this.setState({
			...this.state,
			selectedUser: selectedUser,
		});
		this.fetchEvents();
	}

	render() {
		const year = moment(this.state.currentWeek).format("YYYY"),
			month = moment(this.state.currentWeek).format("MMMM");

		return (
			<table>
				<thead>
					<tr>
						<th colSpan={this.getCurrentWeek().length}>
							<h3>{month + " " + year}</h3>
							<nav aria-label="Page navigation example  d-flex">
								<ul className="pagination justify-content-center">
									<li className="page-item">
										<a
											className="page-link"
											onClick={this.prevWeek}
											aria-label="Previous"
											role="button"
										>
											<span aria-hidden="true" className="text-secondary">
												&laquo;
											</span>
										</a>
									</li>
									<li className="page-item disabled">
										<a className="page-link  text-secondary">
											Week{" "}
											{moment(this.state.currentWeek)
												.add(-1, "week")
												.get("week")}
										</a>
									</li>
									<li className="page-item">
										<a
											className="page-link"
											onClick={this.nextWeek}
											aria-label="Next"
											role="button"
										>
											<span aria-hidden="true" className="text-secondary">
												&raquo;
											</span>
										</a>
									</li>
								</ul>
							</nav>
							<Link to="/add-event">
								<button className="btn btn-outline-dark addNewEventButton float-end m-1">
									Add event
								</button>
							</Link>
							<Link to="/manage-user">
								<button className="btn btn-outline-secondary addNewEventButton float-end m-1">
									Manage user
								</button>
							</Link>
							<span>
								<SelectOneUser
									users={this.state.users}
									selected={this.state.selectedUser}
									handleChange={this.handleSelectedUserChange}
									className="w-25 m-auto mb-4 small"
								/>
							</span>
						</th>
					</tr>
				</thead>
				<tbody>
					<Table
						data={this.state.data}
						weekDays={this.getCurrentWeek()}
						hours={HOURS}
						currentWeek={this.state.currentWeek}
						fetchEvents={this.fetchEvents}
					/>
				</tbody>
			</table>
		);
	}
}

export default WeekPicker;
