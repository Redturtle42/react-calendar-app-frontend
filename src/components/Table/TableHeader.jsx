import React, { Component } from "react";

class TableHeader extends Component {
	render() {
		return (
			<tr>
				{this.props.weekDays.map((day) => (
					<th key={day}>{day}</th>
				))}
			</tr>
		);
	}
}

export default TableHeader;
