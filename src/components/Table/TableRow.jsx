import React, { Component } from "react";
import moment from "moment";
import TableData from "./TableData";

class TableRow extends Component {
	render() {
		let rows = [];

		for (let i = 0; i < 5; i++) {
			const date = moment(this.props.currentWeek).add(i, "d");
			rows.push(
				<TableData
					year={date.get("year")}
					month={date.get("month")}
					day={date.get("date")}
					hour={this.props.hour}
					currentWeek={this.props.currentWeek}
					data={this.props.data}
					key={`${this.props.currentWeek} - ${i}`}
					fetchEvents={this.props.fetchEvents}
				/>
			);
		}
		return (
			<tr>
				<td>{this.props.hour}</td>
				{rows}
			</tr>
		);
	}
}

export default TableRow;
