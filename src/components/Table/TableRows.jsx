import React, { Component, Fragment } from "react";
import TableRow from "./TableRow";

class TableRows extends Component {
	render() {
		return (
			<Fragment>
				{this.props.hours.map((hour) => (
					<TableRow
						key={hour}
						data={this.props.data}
						hour={hour}
						currentWeek={this.props.currentWeek}
						fetchEvents={this.props.fetchEvents}
					/>
				))}
			</Fragment>
		);
	}
}

export default TableRows;
