import React, { Component } from "react";
import Select from "react-select";

const HOURS = [
	{ label: "9-10", value: 9 },
	{ label: "10-11", value: 10 },
	{ label: "11-12", value: 11 },
	{ label: "12-13", value: 12 },
	{ label: "13-14", value: 13 },
	{ label: "14-15", value: 14 },
	{ label: "15-16", value: 15 },
	{ label: "16-17", value: 16 },
	{ label: "17-18", value: 17 },
	{ label: "18-19", value: 18 },
];

class SelectHour extends Component {
	render() {
		const { selected } = this.props.selected;
		return (
			<Select
				value={this.props.eventDate ? selected : null}
				onChange={this.props.handleChange}
				options={HOURS}
				placeholder="Select time..."
				className=" w-50  m-auto mb-4 "
			/>
		);
	}
}

export default SelectHour;
