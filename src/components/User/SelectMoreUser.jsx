import React, { Component } from "react";
import MultiSelect from "react-multi-select-component";

class SelectMoreUser extends Component {
	render() {
		const users = this.props.users.map((item) => {
			return { value: item._id, label: item.name };
		});

		return (
			<div>
				<MultiSelect
					options={users}
					value={this.props.selected}
					onChange={this.props.handleChange}
					overrideStrings={{ selectSomeItems: "Select user..." }}
					className=" w-50  m-auto mb-4 "
					required
				/>
			</div>
		);
	}
}

export default SelectMoreUser;
