import React, { Component } from "react";
import Select from "react-select";

class SelectOneUser extends Component {
	render() {
		const users = this.props.users.map((item) => {
			return { value: item._id, label: item.name };
		});
		if (this.props.users.length >= 1 && !this.props.inUserPage) {
			users.unshift({ value: "", label: "All users..." });
		}

		return (
			<Select
				value={this.props.selected}
				onChange={this.props.handleChange}
				options={users}
				placeholder={this.props.selected || "Select user..."}
				className={this.props.className}
			/>
		);
	}
}

export default SelectOneUser;
