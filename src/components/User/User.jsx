import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import UserPage from "./UserPage";

class User extends Component {
	render() {
		return (
			<Fragment>
				<UserPage />
				<Link to="/">
					<button className="btn btn-outline-secondary">Back</button>
				</Link>
			</Fragment>
		);
	}
}

export default User;
