import React, { Component, Fragment } from "react";
import { getUsers, addUsers, removeUsers } from "../../service/userService";
import { getEvents } from "../../service/eventService";
import SelectOneUser from "./SelectOneUser";

const defaultState = {
	users: [],
	selectedUser: "",
	usersName: "",
	isStateChange: false,
	event: null,
};

class UserPage extends Component {
	constructor(props) {
		super(props);
		this.state = defaultState;

		this.handleUserNameChange = this.handleUserNameChange.bind(this);
		this.handleSelectedUserChange = this.handleSelectedUserChange.bind(this);
		this.deleteUser = this.deleteUser.bind(this);
		this.fetchEvents = this.fetchEvents.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	fetchUsers = () => {
		getUsers().then((data) => {
			this.setState({ ...this.state, users: data });
		});
	};

	fetchEvents = () => {
		getEvents().then((event) => {
			this.setState({ ...this.state, event: event });
		});
	};

	componentDidMount() {
		this.fetchUsers();
		this.fetchEvents();
	}

	deleteUser() {
		const hasEvent = !!this.state.event?.find((event) =>
			event.users.includes(this.state.selectedUser.value)
		);

		if (hasEvent) {
			return alert(
				"As long as the user is attending an event you cannot delete it!"
			);
		}

		removeUsers(this.state.selectedUser.value).then(() => {
			this.setState(defaultState);
			this.fetchUsers();
			alert("User deleted successfully!");
		});
	}

	handleUserNameChange(event) {
		this.setState({ ...this.state, usersName: event.target.value });
	}

	handleSelectedUserChange(selectedUser) {
		this.setState({
			...this.state,
			selectedUser: selectedUser,
		});
	}

	handleSubmit = async () => {
		if (
			this.state.usersName ===
			this.state.users
				.map((i) => i.name)
				.find((item) => item === this.state.usersName)
		) {
			return alert("User already exist!");
		}
		if (this.state.usersName === " ") {
			return alert("User name cannot be empty!");
		}

		if (this.state.usersName !== "") {
			alert("User added!");
		}

		await addUsers(this.state.usersName);
		this.setState(defaultState);
		this.fetchUsers();
		this.setState({ ...this.state, isStateChange: false });
	};

	render() {
		return (
			<Fragment>
				<h3>Manage users</h3>
				<section>
					<form onSubmit={this.handleSubmit}>
						<h4>Add new user:</h4>
						<input
							type="text"
							value={this.state.usersName}
							onChange={this.handleUserNameChange}
							className="form-control w-50"
						/>
						<input
							type="submit"
							value="Submit"
							className="btn btn-success user-name"
						/>
					</form>
				</section>
				<section>
					<span>
						<h4>Remove user:</h4>
						<SelectOneUser
							inUserPage={true}
							users={this.state.users}
							selected={this.state.selectedUser}
							handleChange={this.handleSelectedUserChange}
							className="w-50 m-auto mb-4"
						/>

						<button
							onClick={() => this.deleteUser()}
							className="btn btn-danger mb-4"
						>
							Remove user
						</button>
					</span>
				</section>
			</Fragment>
		);
	}
}

export default UserPage;
