import React, { Component, Fragment } from "react";
import moment from "moment";
import { getUsers } from "../../service/userService";
import { addEvents, getEvents } from "../../service/eventService";
import SelectMoreUser from "../User/SelectMoreUser";
import SelectHour from "../User/SelectHour";

const defaultState = {
	users: [],
	eventTitle: "",
	participants: [],
	eventDate: "",
	selectedTimeSlot: "",
	hasEvent: null,
	isStateChange: false,
};

class EventForm extends Component {
	constructor(props) {
		super(props);
		this.state = defaultState;

		this.handleEventTitleChange = this.handleEventTitleChange.bind(this);
		this.handleParticipantsChange = this.handleParticipantsChange.bind(this);
		this.handleEventDateChange = this.handleEventDateChange.bind(this);
		this.handleHoursChange = this.handleHoursChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentDidMount() {
		getUsers().then((user) => {
			this.setState({ ...this.state, users: user });
		});
		getEvents().then((event) => {
			this.setState({ ...this.state, hasEvent: event });
		});
	}

	handleEventTitleChange(event) {
		this.setState({ ...this.state, eventTitle: event.target.value });
	}

	handleParticipantsChange(participants) {
		this.setState({
			...this.state,
			participants: participants,
		});
	}

	handleEventDateChange(event) {
		this.setState({
			...this.state,
			eventDate: event.target.value,
		});
	}

	handleHoursChange(hour) {
		this.setState({
			...this.state,
			selectedTimeSlot: hour,
		});
	}

	handleSubmit = async (event) => {
		const users = this.state.participants.map((id) => id.value);
		const title = this.state.eventTitle;
		const [year, month, day] = this.state.eventDate.split("-");
		const hour = this.state.selectedTimeSlot.value;

		if (users.length < 1 || !title || !this.state.eventDate || !hour) {
			event.preventDefault();
			return alert("Please fill all the fields.");
		}

		if (title === " ") {
			return alert("Title cannot be empty!");
		}

		const eventDate = moment
			.utc([year, month - 1, day, hour])
			.format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");

		if (day === moment().format("DD") && hour - 1 < moment().format("HH")) {
			event.preventDefault();
			return alert("Please choose a future date!");
		}

		const fetchEventsDate = this.state.hasEvent.map((event) => {
			return event.date;
		});

		if (fetchEventsDate.includes(eventDate)) {
			event.preventDefault();
			return alert(
				"Another event is exist in this time. Room is reserved! Please choose another timeslot!"
			);
		}

		await addEvents(users, title, eventDate);

		getUsers().then((data) => {
			this.setState({ ...defaultState, users: data });
		});
		alert("Event added!");
	};

	render() {
		return (
			<Fragment>
				<h3>Events</h3>
				<section>
					<form onSubmit={this.handleSubmit}>
						<h4>Add new event name:</h4>
						<input
							type="text"
							value={this.state.eventTitle}
							onChange={this.handleEventTitleChange}
							className="form-control w-50"
						/>
						<hr />
						<h4>Select participants:</h4>
						<SelectMoreUser
							users={this.state.users}
							selected={this.state.participants}
							handleChange={this.handleParticipantsChange}
						/>
						<hr />
						<h4>Select day:</h4>
						<input
							type="date"
							value={this.state.eventDate}
							onChange={this.handleEventDateChange}
							className="form-control w-50"
							id="date"
							min={moment().format("YYYY-MM-DD")}
						/>
						<hr />
						<h4>Select hour:</h4>
						<SelectHour
							eventDate={this.state.eventDate}
							selected={this.state.selectedTimeSlot}
							handleChange={this.handleHoursChange}
						/>
						<input
							type="submit"
							value="Submit"
							className="btn btn-success user-name"
						/>
					</form>
				</section>
			</Fragment>
		);
	}
}

export default EventForm;
