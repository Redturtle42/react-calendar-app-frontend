import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import EventForm from "./EventForm";

class Event extends Component {
	render() {
		return (
			<Fragment>
				<EventForm />
				<Link to="/">
					<button className="btn btn-outline-secondary mb-4 mt-4">Back</button>
				</Link>
			</Fragment>
		);
	}
}

export default Event;
