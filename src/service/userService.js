const getUsers = async () => {
	const myHeaders = new Headers({
		"Content-Type": "application/json",
		Accept: "application/json",
	});

	const response = await fetch("http://localhost:8080/api/users", {
		headers: myHeaders,
	})
		.then((response) => {
			return response.json();
		})
		.then((data) => Object.values(data)[0])
		.catch(console.error);
	return response;
};

const addUsers = (usersName) => {
	const formData = {
		name: usersName,
	};

	postData(`http://localhost:8080/api/users`, formData)
		.then((data) => console.log(JSON.stringify(data)))
		.catch((error) => console.error(error));

	function postData(url = ``, data = {}) {
		return fetch(url, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(data),
		}).then((response) => response.json());
	}
};

const removeUsers = async (id) => {
	const response = await fetch(`http://localhost:8080/api/users/${id}`, {
		method: "DELETE",
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
		},
	})
		.then((response) => response.json())
		.catch((error) => {
			throw error;
		})
		.then((data) => console.log(JSON.stringify(data)));
	return response;
};

export { getUsers, addUsers, removeUsers };
