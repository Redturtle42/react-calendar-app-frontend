const getEvents = async () => {
	const myHeaders = new Headers({
		"Content-Type": "application/json",
		Accept: "application/json",
	});

	const response = await fetch("http://localhost:8080/api/events", {
		headers: myHeaders,
	})
		.then((response) => {
			return response.json();
		})
		.then((data) => Object.values(data)[0])
		.catch(console.error);
	return response;
};

const addEvents = (users, title, eventDate) => {
	const formData = {
		users: users,
		title: title,
		date: eventDate,
		duration: 3600000,
	};

	postData(`http://localhost:8080/api/events`, formData)
		.then((data) => console.log(JSON.stringify(data)))
		.catch((error) => console.error(error));

	function postData(url = ``, data = {}) {
		return fetch(url, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(data),
		}).then((response) => response.json());
	}
};

const removeEvents = async (id) => {
	const response = await fetch(`http://localhost:8080/api/events/${id}`, {
		method: "DELETE",
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
		},
	})
		.then((response) => response.json())
		.catch((error) => {
			throw error;
		})
		.then((data) => console.log(JSON.stringify(data)));
	return response;
};

export { getEvents, addEvents, removeEvents };
