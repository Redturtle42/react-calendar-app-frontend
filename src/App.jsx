import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./stylesheets/event-user.css";

import WeekPicker from "../src/components/Table/WeekPicker";
import Event from "../src/components/Event/Event";
import User from "./components/User/User";

class App extends Component {
	render() {
		return (
			<div className="App">
				<Router>
					<Switch>
						<Route exact path="/">
							<WeekPicker />
						</Route>
						<Route exact path="/add-event">
							<Event />
						</Route>
						<Route exact path="/manage-user">
							<User />
						</Route>
					</Switch>
				</Router>
			</div>
		);
	}
}

export default App;
