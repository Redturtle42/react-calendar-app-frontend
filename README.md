# React calendar

## _Simple meeting planning calendar_

This calendar was written for a meeting room in an office.

## Features

> - Display planned meetings by title
> - Filter meetings by participants
> - Change between weeks
> - Add new meeting and participants
> - Delete meeting or participants
> - Warning if time slot is reserved for the specific meeting room

## Tech

Used technologies:

- React
- Javascript
- Npm
- HTML5
- CSS3
- Bootstrap

## Installation

```sh
git clone https://Redturtle42@bitbucket.org/Redturtle42/react-calendar-app-frontend.git

npm install

npm start
```

🎈Have fun!!!🪁
